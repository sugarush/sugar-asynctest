About
=====

Sugar Async Test provides an asynchronous test class for the python unittest
module.

Source
------

The project's `source <https://gitlab.com/sugarush/sugar-asynctest>`_ is
available on `GitLab`.

Installation
------------

Sugar Async Test can be installed with pip.

``pip install git+https://gitlab.com/sugarush/sugar-asynctest@master``

Usage
-----

.. code-block:: python

  from sugar_asynctest import AsyncTestCase


  class TestSomethingAsync(AsyncTestCase):

    async def test_something_async(self):
      self.assertTrue(True)
