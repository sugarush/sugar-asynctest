__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-asynctest',
    version='0.0.1',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-asynctest',
    packages=['sugar_asynctest'],
    description='An asynchronous testcase for the unittest module.',
    install_requires=[ ]
)
